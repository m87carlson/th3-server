FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt --no-cache-dir

COPY th3-server.py th3-server.py

EXPOSE 8080
ENTRYPOINT [ "python3","th3-server.py" ]

