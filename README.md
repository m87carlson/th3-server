# About

I decided to utilize some tooling I have not yet had the time to investigate and play around with.

For this project I chose to use Bitbucket. The primary reason being people I work with follow my public Github account.

The next task was to build a Docker image for the python bottle application. The Dockerfile is using a pretty lightweight python3 base image, and we're using pip to install the required dependencies (Bottle, Redis and Gunicorn).

After that, I wanted to be able to push the built image(s) to a public registry. I have a docker hub account already, and this played nicely into Bitbucket's Pipeline feature.

In my current workplace, I have been spoiled by internal teams providing similar hosting and pipeline services and these are tasks I do not have to think about or re-engineer often. It was exciting to be able to use a new and easy pipeline solution.

Now that we have automation behind building the container, and pushing it up to a publicly accessible location, we can move on to deployment and hosting.

# Resources

 * Docker Hub images: https://hub.docker.com/repository/docker/m87carlson/th3-server#
 ** release-4 tag is v0.0.1, release-7 is v0.0.2
 * Bitbucket repo: https://bitbucket.org/m87carlson/th3-server/src/master/
 * Hosted locally with minikube

# Execution

## Start a minikube cluster
```
minikube --memory 4096 --cpus 8 start --vm=true   # I ran this on a macbook pro, ingress will not install unless minikube is in a vm
minikube addons enable ingress                    # Enable nginx based ingress load balancer
minikube dashboard &                              # If you want to see a dashboard
```

## Apply Kustomize
```
kustomize build kustomize/base | kubectl apply -f -
```

## Bump version in python script

```
git checkout -b m87carlson/v2
sed -i -e 's/0.0.1/0.0.2/'
git add -p th3-server.py                          # I chose to only add in the 0.0.2 line
git commit -m "New version!!"
git push origin a87carlson/v2 
```

PR "approved" and merged:
https://bitbucket.org/m87carlson/th3-server/pull-requests/2/m87carlson-v2


## Re-deploying / Verification

I updated the release tag from 4 to 7. 

Shell loops and curl are all we need:
```
while true; do curl 192.168.64.3:8080/version ; sleep 2; done;
```

![](tty.gif)
